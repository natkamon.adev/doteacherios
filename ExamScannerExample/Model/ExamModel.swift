//
//  ExamModel.swift
//  ExamScannerExample
//
//  Created by Nisit Sirimarnkit on 21/3/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

import Foundation
import ObjectMapper

class ExamModel: Mappable {
    
    var id: String?
    var examname: String?
    var courseid: String?
    var examtype: String?
    var isactive: String?
    var score_type: String?
    var total_score: String?
    var array_answer: [Int]?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        id <- map["id"]
        examname <- map["examname"]
        courseid <- map["courseid"]
        examtype <- map["examtype"]
        isactive <- map["isactive"]
        score_type <- map["score_type"]
        total_score <- map["total_score"]
        
        // 1: Get the value of JSON and store it in a variable of type Any?
        var examData: String?
        var arrayTemp = [Int]()
        switch examtype {
        case "50":
            for i in 1...50 {
                examData <- map["a\(i)"]
                arrayTemp.append(Int(examData!) ?? 0)
            }
        case "100":
            for i in 1...100 {
                examData <- map["a\(i)"]
                arrayTemp.append(Int(examData!) ?? 0)
            }
        default:
            print("default")
        }
        
        
        // 2: Check the value type of the value and convert to Int type
        array_answer = arrayTemp
    }
}
