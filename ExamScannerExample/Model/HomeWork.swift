//
//  HomeWork.swift
//  ExamScannerExample
//
//  Created by March Natkamon on 16/10/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//
//"id": "60",
//"homeworkName": "33333333",
//"due_date": "2019-10-19",
//"course_name": "คณิต",
//"isdel": "y"

import Foundation
import ObjectMapper

class HomeWorkModel: Mappable {
    var id: String?
    var homeworkName: String?
    var due_date: String?
    var course_name: String?
    var isdel: String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        id <- map["id"]
        homeworkName <- map["homeworkName"]
        due_date <- map["due_date"]
        course_name <- map["course_name"]
        isdel <- map["isdel"]
    }
}
