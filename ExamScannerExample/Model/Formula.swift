//
//  Formula.swift
//  ExamScannerExample
//
//  Created by March Natkamon on 16/10/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//
//"id": "3",
//"title": "hHHg",
//"detail": "http://47.74.244.86/doedu/Doc_Vocab/343573.jpg",
//"IsActive": "y"

import Foundation
import ObjectMapper

class FormulaModel: Mappable {
    var id: String?
    var title: String?
    var detail: String?
    var isActive: String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        detail <- map["detail"]
        isActive <- map["IsActive"]
    }
}
