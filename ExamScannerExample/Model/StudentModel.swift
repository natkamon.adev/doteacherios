//
//  File.swift
//  ExamScannerExample
//
//  Created by Adev on 11/10/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

//"student_id": "000001",
//"fname": "ธัญลักษณ์   ",
//"lname": "ทองขาว",
//"createdate": "10/11/2019 10:30:16 AM",
//"chktype": "คณิตศาสตร์"

import Foundation
import ObjectMapper

class StudentModel: Mappable {
    var student_id: String?
    var fname: String?
    var lname: String?
    var createdate: String?
    var chktype: String?
    var class_status: String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        student_id <- map["student_id"]
        fname <- map["fname"]
        lname <- map["lname"]
        createdate <- map["createdate"]
        chktype <- map["chktype"]
        class_status <- map["class_status"]
    }
}

