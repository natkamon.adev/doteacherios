//
//  NewsModel.swift
//  ExamScannerExample
//
//  Created by Nisit Sirimarnkit on 15/3/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//"newsid": "63",
//"title": "ความพึงพอใจกิจกรรมกีฬาสี ประจำปี 2562",
//"news_img": "http://47.74.244.86/du_service_preadmin/img/b.jpg",
//"news_detail": "22",
//"IsActive": "y",
//"schoolid": "2",
//"returncode": "1000",
//"desc": "success",
//"date1": "30/09/2019",
//"news_type": "survey"
//

import Foundation
import ObjectMapper

class NewsModel: Mappable {
    var IsActive: String?
    var desc: String?
    var news_detail: String?
    var news_img: String?
    var newsid: String?
    var returncode: String?
    var schoolid: String?
    var title: String?
    var date1: String?
    var news_type: String?
    
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        IsActive    <- map["IsActive"]
        desc    <- map["desc"]
        news_detail <- map["news_detail"]
        news_img <- map["news_img"]
        newsid <- map["newsid"]
        returncode <- map["returncode"]
        schoolid <- map["schoolid"]
        title <- map["title"]
        date1 <- map["date1"]
        news_type <- map["news_type"]
    }
}
