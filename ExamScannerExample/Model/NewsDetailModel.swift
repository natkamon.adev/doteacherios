//
//  NewsDetailModel.swift
//  ExamScannerExample
//
//  Created by Nisit Sirimarnkit on 17/3/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

import Foundation
import ObjectMapper

class NewsDetailModel: Mappable {
    var newsid: String?
    var title: String?
    var news_detail: String?
    var news_img: String?
    var IsActive: String?
    var schoolid: String?
    var returncode: String?
    var desc: String?
    var date: String?
    var news_type: String?
    
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        newsid    <- map["newsid"]
        title    <- map["title"]
        news_detail <- map["news_detail"]
        news_img <- map["news_img"]
        IsActive <- map["IsActive"]
        schoolid <- map["schoolid"]
        returncode <- map["returncode"]
        desc <- map["desc"]
        date <- map["date1"]
        news_type <- map["news_type"]
    }
}
