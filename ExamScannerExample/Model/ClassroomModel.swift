//
//  ClassroomModel.swift
//  ExamScannerExample
//
//  Created by Nisit Sirimarnkit on 19/3/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

import Foundation
import ObjectMapper

class ClassroomModel: Mappable {
    var teacherid : String?
    var roomid : String?
    var class_ta : String?
    var courseid : String?
    var isEng : String?
    
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        teacherid    <- map["teacherid"]
        roomid    <- map["roomid"]
        class_ta <- map["class_ta"]
        courseid <- map["courseid"]
        isEng <- map["IsEng"]
        
    }
}
