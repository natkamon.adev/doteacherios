//
//  UserModel.swift
//  ExamScannerExample
//
//  Created by Nisit Sirimarnkit on 13/3/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

//"personid": "2",
//"fname": "Pimchan",
//"lname": "Assawanikothon",
//"personal_id": "1101400537221",
//"schoolid": "1",
//"schoolname": "Do Education School",
//"school_img": "http://47.74.244.86/du_service/img/logo.png",
//"person_img": "http://47.74.244.86/du_service_teacher/img/other/ta2.jpg",
//"course_name": null,
//"IsEng": null,
//"returncode": "1000",
//"desc": "Sign in success"

import Foundation
import ObjectMapper

class UserModel: Mappable {
    var personid: String?
    var fname: String?
    var lname: String?
    var personal_id: String?
    var schoolid: String?
    var schoolname: String?
    var school_img: String?
    var person_img: String?
    var course_name: String?
    var returncode: String?
    var IsEng: String?
    var desc: String?
    
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        personid <- map["personid"]
        fname    <- map["fname"]
        lname    <- map["lname"]
        personal_id <- map["personal_id"]
        schoolid <- map["schoolid"]
        schoolname <- map["schoolname"]
        school_img <- map["school_img"]
        person_img <- map["person_img"]
        course_name <- map["course_name"]
        returncode <- map["returncode"]
        IsEng <- map["IsEng"]
        desc <- map["desc"]
    }
}

