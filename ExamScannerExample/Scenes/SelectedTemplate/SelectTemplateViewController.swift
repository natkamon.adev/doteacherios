//
//  SelectTemplateViewController.swift
//  ExamScannerExample
//
//  Created by xAdmin on 13/11/2561 BE.
//  Copyright © 2561 xAdmin. All rights reserved.
//

import UIKit

class SelectTemplateViewController: UIViewController {
    
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var scanButtonView: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scanButtonView.layer.cornerRadius = 4
        scanButtonView.layer.masksToBounds = true
    }
    
    
    @IBAction func scanBtn(_ sender: Any) {
        switch segment.selectedSegmentIndex {
        case 0:
            print("100")
            let controller = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ExamScannerViewController") as? ExamScannerViewController
            controller?.is50Choices = true
            self.navigationController?.pushViewController(controller!, animated: true)
        case 1:
            print("200")
            let controller = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ExamScannerViewController") as? ExamScannerViewController
            controller?.is50Choices = false
            self.navigationController?.pushViewController(controller!, animated: true)
        default:
            print("1")
        }
    }
}
