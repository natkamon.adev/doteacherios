//
//  ExamScannerViewController.swift
//  ExamScannerExample
//
//  Created by xAdmin on 29/10/2561 BE.
//  Copyright © 2561 xAdmin. All rights reserved.
//

import UIKit
import ExamScanner
import AVFoundation

class ExamScannerViewController: UIViewController {
  
    // Mark Outlet
    @IBOutlet weak var scannerBufferImageView: UIImageView!
    
    // Mark Property
    var cameraBuffer: CameraBuffer!
    var examScanner: ExamScanner!
    var is50Choices: Bool = false
    var isScannerSuccess: Bool = false
    var answer:[Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cameraBuffer = CameraBuffer()
        cameraBuffer.delegate = self
        
        let examConfig = ScanConfig()
        if(is50Choices){
            examConfig.answers = answer as [NSNumber]
//                [1, 4, 0, 3, 1, 1, 4, 0, 3, 1, 1, 4, 0, 3, 1, 1, 4, 0, 3, 1, 1, 4, 0, 3, 1, 1, 4, 0, 3, 1, 1, 4, 0, 3, 1, 1, 4, 0, 3, 1, 1, 4, 0, 3, 1, 1, 4, 0, 3, 1]
            examConfig.templateType = .exam_50_choice
        }else{
            examConfig.answers = answer as [NSNumber]
//                [1, 4, 0, 3, 1, 1, 4, 0, 3, 1, 1, 4, 0, 3, 1, 1, 4, 0, 3, 1, 1, 4, 0, 3, 1, 1, 4, 0, 3, 1, 1, 4, 0, 3, 1, 1, 4, 0, 3, 1, 1, 4, 0, 3, 1, 1, 4, 0, 3, 1, 1, 4, 0, 3, 1, 1, 4, 0, 3, 1, 1, 4, 0, 3, 1, 1, 4, 0, 3, 1, 1, 4, 0, 3, 1, 1, 4, 0, 3, 1, 1, 4, 0, 3, 1, 1, 4, 0, 3, 1, 1, 4, 0, 3, 1, 1, 4, 0, 3, 1]
            examConfig.templateType = .exam_100_choice
        }
        self.answer = examConfig.answers as! [Int]
        examScanner = ExamScanner(config: examConfig)
        examScanner.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        isScannerSuccess = false
    }
    
    func pushtoPreviewExamResult(examImage: UIImage, studentID: String, score: String, studentResponse: [Int]) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ExamPreviewViewController") as? ExamPreviewViewController
        controller?.previewImage = examImage
        controller?.studentID = studentID
        controller?.score = score
        controller?.primaryAnswer = self.answer
        controller?.studentResponse = studentResponse
        isScannerSuccess = true
        self.navigationController?.pushViewController(controller!, animated: true)
    }
    
    func formattedStudentId(by studentIdArray: [NSNumber])-> String {
        return studentIdArray.reduce("") { (result, each) -> String in
            guard each.intValue != -1 else { return result}
            let newResult = result + each.intValue.description
            return newResult
        }
    }
}

extension ExamScannerViewController: CameraBufferDelegate {
    func captured(image: UIImage) {
        if(isScannerSuccess) {
            scannerBufferImageView.image = image
            return
        }
        guard let info = examScanner.scan(image) else { return }
        scannerBufferImageView.image = info.bufferImage
    }
}

extension ExamScannerViewController: ExamScannerDelegate {
    func examScannerDidSuccessScan(_ sender: ExamScanner, with result: ScanResult) {
        AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
        let resultStudentID:String = formattedStudentId(by: result.resultStudentID)
        let resultScore: String = "\(result.score)/\(result.resultAnswers.count)"
        let resultPreviewImage: UIImage = result.overlayResultImage
        let studentResponse: [Int] = result.resultAnswers as? [Int] ?? []
        let alert = UIAlertController(title: "Student ID: \(resultStudentID)" ,
            message: "Score: \(resultScore)",
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ViewResult", style: .default, handler: { _ in
            self.pushtoPreviewExamResult(examImage: resultPreviewImage,
                                         studentID: resultStudentID,
                                         score: resultScore, studentResponse: studentResponse)}))
        alert.addAction(UIAlertAction(title: "Retry", style: .cancel, handler: {_ in
            self.isScannerSuccess = false
        }))
        if(isScannerSuccess) { return }
        self.present(alert, animated: true)
        isScannerSuccess = true
    }
}
