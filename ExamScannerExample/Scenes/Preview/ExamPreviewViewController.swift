//
//  ExamPreviewViewController.swift
//  ExamScannerExample
//
//  Created by xAdmin on 13/11/2561 BE.
//  Copyright © 2561 xAdmin. All rights reserved.
//

import UIKit

class ExamPreviewViewController: UIViewController {
    
    // Mark Outlet
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var studentIDLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var scoreView: UIView!
    @IBOutlet weak var imagePreviewView: UIView!
    @IBOutlet weak var questionView: UIView!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var questionLable: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    // Mark Property
    var previewImage: UIImage?
    var score: String?
    var studentID: String?
    var studentResponse: [Int] = []
    var primaryAnswer: [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scoreView.layer.cornerRadius = 4
        scoreView.layer.masksToBounds = true
        scoreView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        
        previewImageView.image = previewImage
        studentIDLabel.text = "Student ID : \(studentID ?? "")"
        scoreLabel.text = "Score : \(score ?? "")"
        
        questionLable.text = "Student ID : \(studentID ?? "")"
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    @IBAction func segmentAction(_ sender: Any) {
        switch segment.selectedSegmentIndex {
        case 0:
            imagePreviewView.isHidden = false
            questionView.isHidden = true
        case 1:
            imagePreviewView.isHidden = true
            questionView.isHidden = false
        default:
            imagePreviewView.isHidden = false
            questionView.isHidden = true
            
        }
    }
}

extension ExamPreviewViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExamScannerPreviewTableViewCell", for: indexPath) as! ExamScannerPreviewTableViewCell
        let choice: String = "\(indexPath.row + 1)"
        var primaryAnswerString: String = ""
        var studentResponseString: String = ""
        
        switch self.primaryAnswer[indexPath.row] {
        case 0 :
            primaryAnswerString = "ก"
        case 1 :
            primaryAnswerString = "ข"
        case 2 :
            primaryAnswerString = "ค"
        case 3 :
            primaryAnswerString = "ง"
        case 4 :
            primaryAnswerString = "จ"
        default:
            primaryAnswerString = ""
        }
        
        switch self.studentResponse[indexPath.row] {
        case 0 :
            studentResponseString = "ก"
        case 1 :
            studentResponseString = "ข"
        case 2 :
            studentResponseString = "ค"
        case 3 :
            studentResponseString = "ง"
        case 4 :
            studentResponseString = "จ"
        default:
            studentResponseString = ""
        }
        cell.setupComponent(choice: choice, primaryAnswer: primaryAnswerString, studentResponse: studentResponseString)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return primaryAnswer.count
    }
}


