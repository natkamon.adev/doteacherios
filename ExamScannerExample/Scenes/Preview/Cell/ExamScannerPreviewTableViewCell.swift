//
//  ExamScannerPreviewTableViewCell.swift
//  ExamScannerExample
//
//  Created by xAdmin on 13/11/2561 BE.
//  Copyright © 2561 xAdmin. All rights reserved.
//

import UIKit

class ExamScannerPreviewTableViewCell: UITableViewCell {
    
    // Mark Outlet
    
    @IBOutlet weak var choice: UILabel!
    @IBOutlet weak var primaryAnswer: UILabel!
    @IBOutlet weak var studentResponse: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    // Mark Function
    
    func setupComponent(choice: String, primaryAnswer: String, studentResponse: String) {
        self.choice.text = choice
        self.primaryAnswer.text = primaryAnswer
        self.studentResponse.text = studentResponse
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}

