import UIKit

class MenuTableViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    let listMenu = ["หน้าหลัก","ข่าวสารทั้งหมด","ชั้นเรียนทั้งหมด","เช็คชื่อ Homeroom",
                    "แบบฟอร์มเฉลยข้อสอบ","นักเรียนที่ขอ Reset Password","เยี่ยมบ้านนักเรียน","แบบสอบถาม",
                    "ประชาสัมพันธ์","เพิ่ม ลด คะแนนความประพฤติ","รายชื่อนักเรียนที่หายไประหว่างวัน","แบบประเมิน SDQ","ออกจากระบบ"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initView()
    }
    
    func initView() {
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        
    }
}
extension MenuTableViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath) as! MenuTableViewCell
        cell.menuLabel.font = UIFont.systemFont(ofSize: 15)
        cell.menuLabel.text = listMenu[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            print("หน้าหลัก")
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "UserInfoViewController") as! UserInfoViewController
            self.navigationController?.pushViewController(vc, animated: true)
        case 1:
            print("click row 1")
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "AllNewsViewController") as! AllNewsViewController
            self.navigationController?.pushViewController(vc, animated: true)
        case 2:
            print("click row 2")
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier:
                "AllClassroomViewController") as! AllClassroomViewController
            self.navigationController?.pushViewController(vc, animated: true)
        case 3:
            print("Click row 3")
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "ChkHomeroomViewController") as! ChkHomeroomViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 4:
            print("Click row 4")
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "SetAnswerViewController") as! SetAnswerViewController
        self.navigationController?.pushViewController(vc, animated: true)
            
        case 5:
            print("Click row 5")
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "ResetPasswordViewController") as! ResetPasswordViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 6:
            print("Click row 6")
//            VisitStudentViewController
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "VisitStudentViewController") as! VisitStudentViewController
            self.navigationController?.pushViewController(vc, animated: true)
        case 7:
            print("Click row 7")
//            QuestionnaireViewController
            
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "QuestionnaireViewController") as! QuestionnaireViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 8:
            print("Click row 8")
//            AdvertiseViewController
            
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "AdvertiseViewController") as! AdvertiseViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 9:
            print("Click row 9")
//            UpDownScoreViewController
        
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "UpDownScoreViewController") as! UpDownScoreViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 10:
            print("Click row 10")
//            DisappearViewController
            
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "DisappearViewController") as! DisappearViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 11:
            print("Click row 11")
//            SdqViewController
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "SdqViewController") as! SdqViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        default:
             print("Click Logout")
//            LoginViewController
             let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
             let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
             self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
}
