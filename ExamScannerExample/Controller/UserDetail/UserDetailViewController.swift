//
//  UserDetailViewController.swift
//  ExamScannerExample
//
//  Created by Nisit Sirimarnkit on 19/3/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class UserDetailViewController: UIViewController {

    
    @IBOutlet var schoolLogoImageView: UIImageView!
    
    @IBOutlet var userImageView: ImageViewCircle!
    
    @IBOutlet var nameLabel: UILabel!
    
    @IBOutlet var schoolLabel: UILabel!
    
    @IBAction func clickChangePass(_ sender: Any) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //    let url = "http://47.74.244.86/du_service_teacher/api/getTop4News"
    var userObject: UserModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        userObject = getDataUserDefault()
        initView()
    }
    
    func getDataUserDefault() -> UserModel {
        let jsonData = UserDefaults.standard.object(forKey: "JSON_DATA")
        let user = Mapper<UserModel>().map(JSON: jsonData as! [String : Any])
        return user!
    }
    
    func initView() {
        let logoImage = URL(string: (userObject?.school_img)!)
        let userImage = URL(string: (userObject?.person_img)!)

        schoolLogoImageView.kf.setImage(with: logoImage, placeholder: UIImage(named: "logo"), options: [
            .transition(.fade(1))
        ])
        
        userImageView.kf.setImage(with: userImage,
                                  placeholder: UIImage(named: "logo"),
                                  options: [
                                    .transition(.fade(1))
        ])
        
        nameLabel.text = "อาจารย์ : \((userObject?.fname)!) \((userObject?.lname)!)"
        schoolLabel.text = "โรงเรียน : \((userObject?.schoolname)!)"
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
