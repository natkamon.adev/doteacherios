//
//  ChangePasswordViewController.swift
//  ExamScannerExample
//
//  Created by Adev on 13/9/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire
import AlamofireObjectMapper
import Kingfisher

class ChangePasswordViewController: UIViewController {

    var userObject: UserModel?
    var url = "http://47.74.244.86/du_service_teacher/api/change_password"
    
    @IBOutlet var usernameTextField: UITextField!
    
    @IBOutlet var telTextField: UITextField!
    
    @IBOutlet var newPassTextfield: UITextField!
    
    
    @IBAction func clickConfirm(_ sender: Any) {
        let user = usernameTextField.text
        let tel = telTextField.text
        let newPass = newPassTextfield.text
        
        print("user: \(user!), tel: \(tel!), new password: \(newPass!)")
        
        callLoginWithAlamofire(user: user!, tel: tel!, newPass: newPass!) { (response, status) in
                
            self.userObject = response as? UserModel
            let jsonData = self.userObject?.toJSON()
            print("jsonData = \(jsonData!)")
            
//            UserDefaults.standard.set(jsonData, forKey: "JSON_DATA")
            if (status == true && (self.userObject!.returncode == "1000")) {
                print("success")
                let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "UserDetailViewController") as! UserDetailViewController
//                self.navigationController?.pushViewController(vc, animated: true)
                self.present(vc, animated: true, completion: nil)

            }
            else {
                let alertController = UIAlertController(title: "เกิดข้อผิดพลาด", message: "ข้อมูลไม่ถูกต้อง", preferredStyle: .alert)

                let action1 = UIAlertAction(title: "ตกลง", style: .default) { (action:UIAlertAction) in
                    print("You've pressed default");
                }

                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
            }
            
        }
    }
    
    func callLoginWithAlamofire(user: String, tel: String, newPass: String, completionHandler:@escaping (_ isResponse:Any,Bool)-> Void) {
        let parameters: Parameters = ["username": user,"tel": tel, "newpassword": newPass]
        
        AF.request(url,method: .post,parameters: parameters).validate().responseObject { (response:DataResponse<UserModel>) in
            
            switch response.result {
            case .success:
                print("Validation Successful")
                if let object = response.result.value {
                    completionHandler(object,true)
                }
            case .failure(let error):
                completionHandler(error,false)
                print(error)
            }
            
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
