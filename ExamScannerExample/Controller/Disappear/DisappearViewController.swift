//
//  DisappearViewController.swift
//  ExamScannerExample
//
//  Created by March Natkamon on 8/9/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire
import SideMenu


class DisappearViewController: UIViewController {
    
    var userObject: UserModel?
    var studentArray = [StudentModel]()
    let url = "http://47.74.244.86/du_service_teacher/api/getAbsence"
    

    @IBOutlet var userImageView: ImageViewCircle!
    
    @IBOutlet var studentTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        userObject = getDataUserDefault()
        initView()
        
    }
    
    func getDataUserDefault() -> UserModel {
            let jsonData = UserDefaults.standard.object(forKey: "JSON_DATA")
            let user = Mapper<UserModel>().map(JSON: jsonData as! [String: Any])
            return user!
    }
    
    func initView() {
        studentTableView.delegate = self
        studentTableView.dataSource = self
        
        let urlUserImage = URL(string: (userObject?.person_img)!)
        userImageView.kf.setImage(with: urlUserImage, placeholder: UIImage(named: "logo"), options: [
            .transition(.fade(1)),
            .cacheOriginalImage
        ])
        
        callStudentDisappearWithAlamofire(){
            (response, status) in
            
            self.studentArray = response as! [StudentModel]
            self.studentTableView.reloadData()
        }
        
//        print("initView End")
        
    }
    
    func callStudentDisappearWithAlamofire(completionHandler: @escaping ((Any, Bool) -> Void)){
        
        AF.request(url, method: .post).validate().responseArray {(response: DataResponse<[StudentModel]>) in
            
            switch response.result {
            case .success:
                print("Validation Successful")
                if let objectArray = response.result.value{
                    completionHandler(objectArray, true)
                }
            case .failure(let error):
                completionHandler(error, false)
                print(error)
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DisappearViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        print("studentArray.count = \(studentArray.count)")
        return studentArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath) as! DisappearTableViewCell
//        print("tableView")
        let studentText = "\(studentArray[indexPath.row].student_id!) : \(studentArray[indexPath.row].fname!)  \(studentArray[indexPath.row].lname!)"
        let subjectText = "คาบเรียนที่หายไป : \(studentArray[indexPath.row].chktype!)"
        let dateText = "วันที่/เวลา : \(studentArray[indexPath.row].createdate!)"
//        print("student_id = \(studentArray[indexPath.row].student_id!)")
        
        cell.studentLabel.text = studentText
        cell.subjectLabel.text = subjectText
        cell.dateLabel.text = dateText
        
        return cell
    }
    
    
}
