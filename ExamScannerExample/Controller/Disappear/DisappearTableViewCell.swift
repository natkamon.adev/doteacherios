//
//  DisappearTableViewCell.swift
//  ExamScannerExample
//
//  Created by Adev on 8/10/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

import UIKit

class DisappearTableViewCell: UITableViewCell {

    @IBOutlet var studentLabel: UILabel!
    @IBOutlet var subjectLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    
    @IBOutlet var cellBackground: DesignableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
