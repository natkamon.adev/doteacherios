//
//  HomeworkTableViewCell.swift
//  ExamScannerExample
//
//  Created by March Natkamon on 16/10/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

import UIKit

class HomeworkTableViewCell: UITableViewCell {

    @IBOutlet var cellbackground: DesignableView!
    
    @IBOutlet var homeworkLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
