//
//  AttendanceTableViewCell.swift
//  ExamScannerExample
//
//  Created by Adev on 16/10/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

import UIKit

class AttendanceTableViewCell: UITableViewCell {

    @IBOutlet var studentLabel: UILabel!
    @IBOutlet var statusImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
