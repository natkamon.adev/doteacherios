//
//  AttendanceViewController.swift
//  ExamScannerExample
//
//  Created by Adev on 13/9/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire
import SideMenu

class AttendanceViewController: UIViewController {

    @IBOutlet var studentTableView: UITableView!
    
    @IBOutlet var userImageView: ImageViewCircle!
    
    let url = "http://47.74.244.86/du_service_teacher/api/getStatusStd"
    var userObject: UserModel?
    var courseId: String?
    var roomId: String?
    var studentArray = [StudentModel]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userObject = getDataUserDefault()
        initView()
        // Do any additional setup after loading the view.
    }
    
    func getDataUserDefault() -> UserModel {
        let jsonData = UserDefaults.standard.object(forKey: "JSON_DATA")
        let user = Mapper<UserModel>().map(JSON: jsonData as! [String: Any])
        return user!
    }
    
    func initView() {
        //        print("initView start")
        studentTableView.delegate = self
        studentTableView.dataSource = self
        
        let urlUserImage = URL(string: (userObject?.person_img)!)
        userImageView.kf.setImage(with: urlUserImage, placeholder: UIImage(named: "logo"),options: [
            .transition(.fade(1)),
            .cacheOriginalImage
            ])
        
        let teacherIdString = userObject?.personid
//        print("teacher id = \(teacherIdString!), room id = \(String(describing: roomId!)), course id = \(String(describing: courseId!))")
        callStudentServiceWithAlamofire(teacherId: teacherIdString!, roomId: roomId!, courseId: courseId!) {
            (response, status) in
            
            self.studentArray = response as! [StudentModel]
            self.studentTableView.reloadData()
        }
//                print("initView end")
    }
    
    func callStudentServiceWithAlamofire(teacherId: String, roomId: String, courseId: String, completionHandler: @escaping ((Any, Bool) -> Void)) {
        let parameters: Parameters = ["teacherid": teacherId, "roomId": roomId, "courseId": courseId]
        
        AF.request(url, method: .post, parameters: parameters).validate().responseArray {
            (response: DataResponse<[StudentModel]>) in
            
            switch response.result {
            case .success:
                print("Validation Successful")
                if let objectArray = response.result.value {
                    completionHandler(objectArray, true)
                }
            case .failure(let error):
                completionHandler(error, false)
                print(error)
            }
        }
        //        print("callClassroomServiceWithAlamofire")
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension AttendanceViewController: UITableViewDelegate, UITableViewDataSource {
    func  tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("studentArray.count = \(studentArray.count)")
        return studentArray.count
    }
    
    func  tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath) as! AttendanceTableViewCell
    
        cell.studentLabel.text = "\(studentArray[indexPath.row].fname!) \(studentArray[indexPath.row].lname!)"
        print("status: \(studentArray[indexPath.row].class_status!)")
        switch studentArray[indexPath.row].class_status! {
        case "2":
            cell.statusImageView.image = UIImage(named: "emotion2.png")
        case "3":
            cell.statusImageView.image = UIImage(named: "emotion3.png")
        default:
            cell.statusImageView.image = UIImage(named: "emotion1.png")
        }
        return cell
    }
}


