//
//  FormulaViewController.swift
//  ExamScannerExample
//
//  Created by March Natkamon on 16/10/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire
import SideMenu

class FormulaViewController: UIViewController {
    var userObject: UserModel?
    var formulaArray = [FormulaModel]()
    let url = "http://47.74.244.86/du_service_teacher/api/getVocab3List"
    var urldelete = "http://47.74.244.86/du_service_teacher/api/delVocab3"
    var courseId: String?
    var roomId: String?
    
    @IBOutlet var userImageView: ImageViewCircle!
    
    @IBOutlet var formulaTableView: UITableView!
    
    @IBAction func clickAddFormula(_ sender: Any) {
        print("clickAddFormula")
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "NewFormulaViewController") as! NewFormulaViewController
//        vc.courseId = classroom?.courseid
//        vc.roomId = classroom?.roomid
        navigationController?.pushViewController(vc, animated: true)

    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        userObject = getDataUserDefault()
        initView()
    }
    
    func getDataUserDefault() -> UserModel {
        let jsonData = UserDefaults.standard.object(forKey: "JSON_DATA")
        let user = Mapper<UserModel>().map(JSON: jsonData as! [String: Any])
        return user!
    }

    func initView() {
        //        print("initView start")
        formulaTableView.delegate = self
        formulaTableView.dataSource = self
        
        let urlUserImage = URL(string: (userObject?.person_img)!)
        //        print("url image = \(String(describing: URL(string: (userObject?.person_img)!)))")
        userImageView.kf.setImage(with: urlUserImage, placeholder: UIImage(named: "logo"),options: [
            .transition(.fade(1)),
            .cacheOriginalImage
            ])
        
        let teacherIdString = userObject?.personid!
        callFormulaServiceWithAlamofire(teacherId: teacherIdString!, roomId: roomId!, courseId: courseId!) {
            (response, status) in
            
            self.formulaArray = response as! [FormulaModel]
            self.formulaTableView.reloadData()
        }
        //        print("initView end")
    }
    
    func callFormulaServiceWithAlamofire(teacherId: String, roomId: String, courseId: String, completionHandler: @escaping ((Any, Bool) -> Void)) {
        let parameters: Parameters = ["teacherid": teacherId, "roomId": roomId, "courseId": courseId]
        
        AF.request(url, method: .post, parameters: parameters).validate().responseArray {
            (response: DataResponse<[FormulaModel]>) in
            
            switch response.result {
            case .success:
                print("Validation Successful")
                if let objectArray = response.result.value {
                    completionHandler(objectArray, true)
                }
            case .failure(let error):
                completionHandler(error, false)
                print(error)
            }
        }
        //        print("callClassroomServiceWithAlamofire")
    }
    
    func calldeleteFormulaWithAlamofire(id: String, completionHandler:@escaping (_ isResponse:Any,Bool)-> Void) {
        print("id in calldeleteFormulaWithAlamofire: \(id)")
        AF.request(urldelete, method: .post, parameters: id).validate().responseObject { (response:DataResponse<FormulaModel>) in
            print("error")
            switch response.result {
            case .success:
                print("Validation Successful")
                if let object = response.result.value {
                    completionHandler(object,true)
                }
            case .failure(let error):
                completionHandler(error,false)
                print(error)
            }
            
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FormulaViewController: UITableViewDelegate, UITableViewDataSource {
    func  tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return formulaArray.count
    }
    
    func  tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath) as! FormulaTableViewCell
        cell.formulaLabel.text = formulaArray[indexPath.row].title
//        let gesture = UITapGestureRecognizer(target: self, action: #selector(clickCellBackground))
//        cell.cellBackground.addGestureRecognizer(gesture)
            let gesture = UITapGestureRecognizer(target: self, action: #selector(clickDelete))
            cell.deleteButton.addGestureRecognizer(gesture)
        return cell
    }
    
//    @objc func clickCellBackground(sender: UITapGestureRecognizer){
//        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ClassroomAnswerSheetViewController") as! ClassroomAnswerSheetViewController
//        //            print("vc = \(vc)")
//
//        let tapLocation = sender.location(in: self.classroomTableView)
//        //            print("tapLocation = \(tapLocation)")
//
//        let indexPath = self.classroomTableView.indexPathForRow(at: tapLocation)
//
//        vc.classroom = classroomArray[(indexPath?.row)!]
//        self.navigationController?.pushViewController(vc, animated: true)
//    }

    @objc func clickDelete(sender: UITapGestureRecognizer){
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "FormulaViewController") as! FormulaViewController
        //            print("vc = \(vc)")

        let tapLocation = sender.location(in: self.formulaTableView)
        //            print("tapLocation = \(tapLocation)")

        let indexPath = self.formulaTableView.indexPathForRow(at: tapLocation)
        let id = formulaArray[indexPath!.row].id!
        print("id: \(id)")


        calldeleteFormulaWithAlamofire(id: id) { (response, status) in

//            self.formulaArray = response as! [FormulaModel]
            self.formulaTableView.reloadData()
        }
        
        vc.courseId = courseId
        vc.roomId = roomId
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}
