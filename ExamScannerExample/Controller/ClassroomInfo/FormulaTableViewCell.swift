//
//  FormulaTableViewCell.swift
//  ExamScannerExample
//
//  Created by March Natkamon on 16/10/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

import UIKit

class FormulaTableViewCell: UITableViewCell {

    @IBOutlet var cellBackground: DesignableView!
    
    @IBOutlet var formulaLabel: UILabel!
    
    @IBOutlet var deleteButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
