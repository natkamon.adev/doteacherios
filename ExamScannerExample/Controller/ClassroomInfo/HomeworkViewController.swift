//
//  HomeworkViewController.swift
//  ExamScannerExample
//
//  Created by Adev on 13/9/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire

class HomeworkViewController: UIViewController {
    
    let url = "http://47.74.244.86/du_service_teacher/api/getHomework"
    var homeworkArray = [HomeWorkModel]()
    var courseId: String?
    var roomId: String?
    var userObject: UserModel?
    
    @IBOutlet var userImageView: ImageViewCircle!
    
    @IBOutlet var homeworkTableView: UITableView!

    @IBAction func allHomework(_ sender: UIButton) {
    }
    @IBAction func addHomework(_ sender: UIButton) {
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        userObject = getDataUserDefault()
    }
    
    func getDataUserDefault() -> UserModel {
        let jsonData = UserDefaults.standard.object(forKey: "JSON_DATA")
        let user = Mapper<UserModel>().map(JSON: jsonData as! [String: Any])
        return user!
    }
    
    func initView() {
        //        print("initView start")
        homeworkTableView.delegate = self
        homeworkTableView.dataSource = self
        
        let urlUserImage = URL(string: (userObject?.person_img)!)
        //        print("url image = \(String(describing: URL(string: (userObject?.person_img)!)))")
        userImageView.kf.setImage(with: urlUserImage, placeholder: UIImage(named: "logo"),options: [
            .transition(.fade(1)),
            .cacheOriginalImage
            ])
        
        let teacherIdString = userObject?.personid
        callHomeworkServiceWithAlamofire(teacherId: teacherIdString!, roomId: roomId!, courseId: courseId!) {
            (response, status) in
            
            self.homeworkArray = response as! [HomeWorkModel]
            self.homeworkTableView.reloadData()
        }
        //        print("initView end")
    }
    
    func callHomeworkServiceWithAlamofire(teacherId: String, roomId: String, courseId: String, completionHandler: @escaping ((Any, Bool) -> Void)) {
        let parameters: Parameters = ["teacherid": teacherId, "roomId": roomId, "courseId": courseId]
        
        AF.request(url, method: .post, parameters: parameters).validate().responseArray {
            (response: DataResponse<[HomeWorkModel]>) in
            
            switch response.result {
            case .success:
                print("Validation Successful")
                if let objectArray = response.result.value {
                    completionHandler(objectArray, true)
                }
            case .failure(let error):
                completionHandler(error, false)
                print(error)
            }
        }
        //        print("callClassroomServiceWithAlamofire")
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HomeworkViewController: UITableViewDelegate, UITableViewDataSource {
    func  tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return homeworkArray.count
    }
    
    func  tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath) as! HomeworkTableViewCell
        cell.homeworkLabel.text = homeworkArray[indexPath.row].homeworkName
        //        let gesture = UITapGestureRecognizer(target: self, action: #selector(clickCellBackground))
        //        cell.cellBackground.addGestureRecognizer(gesture)
//        let gesture = UITapGestureRecognizer(target: self, action: #selector(clickDelete))
//        cell.deleteButton.addGestureRecognizer(gesture)
        return cell
    }
}
