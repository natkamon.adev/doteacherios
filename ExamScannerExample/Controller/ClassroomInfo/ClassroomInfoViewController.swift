//
//  ClassroomInfoViewController.swift
//  ExamScannerExample
//
//  Created by Nisit Sirimarnkit on 19/3/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

import UIKit
import ObjectMapper
import Kingfisher

class ClassroomInfoViewController: UIViewController {

    var classroom : ClassroomModel?
    var userObject: UserModel?
    
    @IBOutlet var userImageView: ImageViewCircle!
    
    @IBOutlet var classroomLabel: UILabel!
    
    @IBOutlet var vocabularyButton: UIButton!
    
    @IBAction func checkExam(_ sender: Any) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ExamViewController") as! ExamViewController
        vc.courseId = classroom?.courseid
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickAttendance(_ sender: Any) {
        print("clickAttendance")
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "AttendanceViewController") as! AttendanceViewController
        vc.courseId = classroom?.courseid
        vc.roomId = classroom?.roomid
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func clickFormula(_ sender: Any) {
        print("clickFormula")
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "FormulaViewController") as! FormulaViewController
        vc.courseId = classroom?.courseid
        vc.roomId = classroom?.roomid
        navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        userObject = getDataUserDefault()
        initView()
        if (classroom?.isEng! == "n"){
//            isnotEng()
        }
        print("classroom info: \(String(describing: classroom?.isEng!))")
    }
    
    func getDataUserDefault() -> UserModel {
        let jsonData = UserDefaults.standard.object(forKey: "JSON_DATA")
        let user = Mapper<UserModel>().map(JSON: jsonData as! [String : Any])
        return user!
    }

    func initView() {
        
        classroomLabel.text = classroom?.class_ta
        let userImage = URL(string: (userObject?.person_img)!)
        userImageView.kf.setImage(with: userImage,
                                  placeholder: UIImage(named: "logo"),
                                  options: [
                                    //.processor(processor),
                                    //.scaleFactor(UIScreen.main.scale),
                                    .transition(.fade(1))
                                    //.cacheOriginalImage
            ])
    }
    
    func isnotEng() {
        print("isnotEng")
        vocabularyButton.setImage(UIImage(named: "iseng_n"), for: .normal)
        vocabularyButton.isEnabled = false;
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
