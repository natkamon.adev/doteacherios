//
//  ExamViewController.swift
//  ExamScannerExample
//
//  Created by Nisit Sirimarnkit on 21/3/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class ExamViewController: UIViewController {

    @IBOutlet var examTableView: UITableView!
    
    @IBOutlet var userImageView: ImageViewCircle!
    
    var courseId: String?
    
    let url = "http://47.74.244.86/du_service_teacher/api/getExamList"
    var userObject : UserModel?
    var examArray = [ExamModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        userObject = getDataUserDefault()
        print("march")
        initView()
        
    }
    
    func getDataUserDefault() -> UserModel {
        let jsonData = UserDefaults.standard.object(forKey: "JSON_DATA")
        let user = Mapper<UserModel>().map(JSON: jsonData as! [String : Any])
        return user!
    }
    
    func initView() {
        examTableView.delegate = self
        examTableView.dataSource = self
        
        let urlUserImage = URL(string: (userObject?.person_img)!)
        userImageView.kf.setImage(with: urlUserImage,
                                  placeholder: UIImage(named: "logo"),
                                  options: [
                                    //.processor(processor),
                                    //.scaleFactor(UIScreen.main.scale),
                                    .transition(.fade(1)),
                                    .cacheOriginalImage
            ])
        
        
        
        callExamServiceAlamofire(courseid: courseId!) { (response, status) in
            self.examArray = response as! [ExamModel]
            //print(self.examArray[0].array_answer)
            self.examTableView.reloadData()
        }
    }

    
    func callExamServiceAlamofire(courseid:String,completionHandler:@escaping ((Any,Bool)->Void)) {
        
        let parameters: Parameters = ["courseid":courseid]
        
        AF.request(url, method: .post,parameters: parameters).validate().responseArray { (response:DataResponse<[ExamModel]>) in
            
            switch response.result {
            case .success:
                print("Validation Successful")
                if let objectArray = response.result.value {
                    //print(objectArray.count)
                    completionHandler(objectArray,true)
                }
            case .failure(let error):
                completionHandler(error,false)
                print(error)
            }
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ExamViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return examArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath) as! ExamTableViewCell
        cell.titleLabel.text = examArray[indexPath.row].examname
        let gesture = UITapGestureRecognizer(target: self, action: #selector(clickBackgroundView))
        cell.cellBackgroundView.addGestureRecognizer(gesture)
        return cell
    }
    
    @objc func clickBackgroundView(sender : UITapGestureRecognizer) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ExamScannerViewController") as! ExamScannerViewController
        //using sender, we can get the point in respect to the table view
        let tapLocation = sender.location(in: self.examTableView)
        
        //using the tapLocation, we retrieve the corresponding indexPath
        let indexPath = self.examTableView.indexPathForRow(at: tapLocation)
        if (examArray[(indexPath?.row)!].examtype == "50") {
            vc.is50Choices = true
            vc.answer = examArray[(indexPath?.row)!].array_answer!
        } else {
            vc.is50Choices = false
            vc.answer = examArray[(indexPath?.row)!].array_answer!
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
