//
//  ExamTableViewCell.swift
//  ExamScannerExample
//
//  Created by Nisit Sirimarnkit on 21/3/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

import UIKit

class ExamTableViewCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var cellBackgroundView: DesignableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
