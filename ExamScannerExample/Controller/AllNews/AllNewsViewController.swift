//
//  AllNewsViewController.swift
//  ExamScannerExample
//
//  Created by Nisit Sirimarnkit on 17/3/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

import UIKit
import FSPagerView
import Alamofire
import ObjectMapper
import Kingfisher

class AllNewsViewController: UIViewController {
    
   
    @IBOutlet var userImageView: ImageViewCircle!
    
    @IBOutlet var allNewsTableView: UITableView!
    
    fileprivate let imageNames = ["1.jpg","2.jpg","3.jpg"]
    let url = "http://47.74.244.86/du_service_teacher/api/getNews"
    var newsArray = [NewsModel]()
    var userObject: UserModel?
    
//    @IBOutlet var pageControl: FSPageControl! {
//        didSet {
//            self.pageControl.numberOfPages = self.imageNames.count
//            self.pageControl.contentHorizontalAlignment = .center
//            self.pageControl.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
//            self.pageControl.hidesForSinglePage = true
//        }
//    }
    
//    @IBOutlet weak var pagerView: FSPagerView! {
//        didSet {
//            self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
//            pagerView.automaticSlidingInterval = 3.0
//        }
//    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        userObject = getDataUserDefault()
        initView()
    }
    
    func getDataUserDefault() -> UserModel {
        let jsonData = UserDefaults.standard.object(forKey: "JSON_DATA")
        let user = Mapper<UserModel>().map(JSON: jsonData as! [String : Any])
        return user!
    }
    
    func initView(){
        
        allNewsTableView.delegate = self
        allNewsTableView.dataSource = self
        
//        self.pagerView.dataSource = self
//        self.pagerView.delegate = self
//        self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        
        // Create a page control
//        self.view.addSubview(self.pageControl)
        
        let schoolIdString = userObject?.schoolid
        let teacherIdString = userObject?.personid
        
//        callTopNewsServiceWithAlamofire(schoolId: schoolIdString!) { (response, status) in
//            self.topNewsArray = response as! [NewsModel]
//             //Create a pager view
//
//           self.pagerView.reloadData()
//        }
        
        
        
        let userUrl = URL(string: (userObject?.person_img)!)
        userImageView.kf.setImage(with: userUrl,
                                  placeholder: UIImage(named: "logo"),
                                  options: [
                                    //.processor(processor),
                                    //.scaleFactor(UIScreen.main.scale),
                                    .transition(.fade(1))
                                    //.cacheOriginalImage
            ])
    
       
        
        callAllNewsServiceWithAlamofire(schoolId: schoolIdString!, teacherId: teacherIdString!) { (response, status) in
            self.newsArray = response as! [NewsModel]
            let jsonData = self.newsArray.toJSON()
            print(jsonData)
            
//            self.userObject = response as? NewsModel
//            let jsonData = self.userObject?.toJSON()
//            print("jsonData = \(jsonData!)")
//            UserDefaults.standard.set(jsonData, forKey: "JSON_DATA")
            
            
            //print("size : \(self.newsArray.count)")
            //print("image : \(self.newsArray[0].news_img)")
            
            self.allNewsTableView.reloadData()
        }
    }
    
//    func callTopNewsServiceWithAlamofire(schoolId: String, completionHandler: @escaping ((Any,Bool)->Void)) {
//        let parameters: Parameters = ["schoolid": schoolId]
//
//        AF.request(url, method: .post,parameters: parameters).validate().responseArray { (response:DataResponse<[NewsModel]>) in
//
//            switch response.result {
//            case .success:
//                print("Validation Successful")
//                if let objectArray = response.result.value {
//                    //print(objectArray.count)
//                    completionHandler(objectArray,true)
//                }
//            case .failure(let error):
//                completionHandler(error,false)
//                print(error)
//            }
//        }
//
//    }

    func callAllNewsServiceWithAlamofire(schoolId: String, teacherId: String, completionHandler: @escaping ((Any,Bool)->Void)) {
        let parameters: Parameters = ["schoolid": schoolId , "advisorid": teacherId]
        
        AF.request(url, method: .post,parameters: parameters).validate().responseArray { (response:DataResponse<[NewsModel]>) in
            
            switch response.result {
            case .success:
                print("Validation Successful")
                if let objectArray = response.result.value {
                    //print(objectArray.count)
                    completionHandler(objectArray,true)
                }
            case .failure(let error):
                completionHandler(error,false)
                print(error)
            }
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//extension AllNewsViewController: FSPagerViewDataSource,FSPagerViewDelegate {
//    public func numberOfItems(in pagerView: FSPagerView) -> Int {
//        print("zzz : \(self.topNewsArray.count)")
//        return self.topNewsArray.count
//    }
//
//    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
//        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
//        //cell.imageView?.image = UIImage(named: self.imageNames[index])
//        print("zzz : \(self.topNewsArray[index].news_img!)")
//        let topImageUrl = URL(string: topNewsArray[index].news_img!)
//        cell.imageView!.kf.setImage(with: topImageUrl,
//                                       placeholder: UIImage(named: "logo"),
//                                       options: [
//                                        //.processor(processor),
//                                        //.scaleFactor(UIScreen.main.scale),
//                                        .transition(.fade(1))
//                                        //.cacheOriginalImage
//            ])
//        cell.imageView?.contentMode = .scaleAspectFill
//        return cell
//    }
    
//    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
//        self.pageControl.currentPage = targetIndex
//    }
//
//    func pagerView(_ pagerView: FSPagerView, willDisplay cell: FSPagerViewCell, forItemAt index: Int) {
//        self.pageControl.currentPage = index
//    }
//}

extension AllNewsViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELLTABLE", for: indexPath) as! AllNewsTableViewCell
        cell.cellTitleLabel.text = newsArray[indexPath.row].title
        cell.cellDetailLabel.text = newsArray[indexPath.row].news_detail
        let newsUrl = URL(string: (newsArray[indexPath.row].news_img!))
        cell.cellImageView.kf.setImage(with: newsUrl,
                                       placeholder: UIImage(named: "logo"),
                                       options: [
                                        //.processor(processor),
                                        //.scaleFactor(UIScreen.main.scale),
                                        .transition(.fade(1))
                                        //.cacheOriginalImage
            ])
        let gesture = UITapGestureRecognizer(target: self, action: #selector(clickCellBackground))
        cell.cellBackgroundView.addGestureRecognizer(gesture)
        
        return cell
    }
    
    @objc func clickCellBackground(sender : UITapGestureRecognizer){
        
//        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//        let vc = mainStoryboard.instantiateViewController(withIdentifier: "NewsDetailViewController") as! NewsDetailViewController
//        let tapLocation = sender.location(in: self.allNewsTableView)
//        let indexPath = self.allNewsTableView.indexPathForRow(at: tapLocation)
//        vc.schoolId = newsArray[(indexPath?.row)!].schoolid
//        vc.newsId = newsArray[(indexPath?.row)!].newsid
//        self.present(vc, animated: true,completion: nil)
        
        let tapLocation = sender.location(in: self.allNewsTableView)
               let indexPath = self.allNewsTableView.indexPathForRow(at: tapLocation)
               
               if(newsArray[indexPath!.row].news_type == "normal"){
                           let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                           let vc = mainStoryboard.instantiateViewController(withIdentifier: "NewsDetailViewController") as! NewsDetailViewController
                           let tapLocation = sender.location(in: self.allNewsTableView)
                           let indexPath = self.allNewsTableView.indexPathForRow(at: tapLocation)
                           vc.schoolId = newsArray[(indexPath?.row)!].schoolid
                           vc.newsId = newsArray[(indexPath?.row)!].newsid
                           self.present(vc, animated: true, completion: nil)
               }else if(newsArray[indexPath!.row].news_type == "survey"){
                           let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                           let vc = mainStoryboard.instantiateViewController(withIdentifier: "WebViewNewsViewController") as! WebViewNewsViewController
                           let tapLocation = sender.location(in: self.allNewsTableView)
                           let indexPath = self.allNewsTableView.indexPathForRow(at: tapLocation)
                           vc.detailNews = newsArray[(indexPath?.row)!].news_detail
                           self.present(vc, animated: true, completion: nil)
               }
        
        
    }
    
    
    
}
