//
//  AllNewsTableViewCell.swift
//  ExamScannerExample
//
//  Created by Nisit Sirimarnkit on 17/3/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

import UIKit

class AllNewsTableViewCell: UITableViewCell {
    
    @IBOutlet var cellBackgroundView: DesignableView!
    @IBOutlet var cellImageView: UIImageView!
    
    @IBOutlet var cellDetailLabel: UILabel!
    
    @IBOutlet var cellTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
