//
//  ClassroomAnswerSheetViewController.swift
//  ExamScannerExample
//
//  Created by Adev on 7/10/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

import UIKit
import ObjectMapper
import SideMenu
import Alamofire

class ClassroomAnswerSheetViewController: UIViewController {

    @IBOutlet var userImageView: ImageViewCircle!
    
    @IBOutlet var answerSheetTableView: UITableView!

//    var courseId: String?
    
    var userObject: UserModel?
    var classroom: ClassroomModel?
    var answerSheetArray = [ExamModel]()
    let url = "http://47.74.244.86/du_service_teacher/api/getExamList"


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        userObject = getDataUserDefault()
        initView()
    }
    
    func getDataUserDefault() -> UserModel {
        let jsonData = UserDefaults.standard.object(forKey: "JSON_DATA")
        let user = Mapper<UserModel>().map(JSON: jsonData as! [String: Any])
        return user!
    }
    
    func initView() {
        print("initView start")
        answerSheetTableView.delegate = self
        answerSheetTableView.dataSource = self
        
        let urlUserImage = URL(string: (userObject?.person_img)!)
        userImageView.kf.setImage(with: urlUserImage, placeholder: UIImage(named: "logo"), options: [
            .transition(.fade(1)),
            .cacheOriginalImage
        ])
        
        let teacherIdString = userObject?.personid
        let schoolIdString = userObject?.schoolid
        let courseIdString = classroom?.courseid

        print("t_id = \(teacherIdString!), s_id = \(schoolIdString!), c_id = \(courseIdString!)")
        callAnswerSheetServiceWithAlamofire(teacherId: teacherIdString!, schoolId: schoolIdString!, courseID: courseIdString!) {
            (response, status) in
            self.answerSheetArray = response as! [ExamModel]
            self.answerSheetTableView.reloadData()
        }
//        print("courseId \(String(describing: courseIdString))")
//
//
//        callAnswerServiceWithAlamofire(courseID: courseIdString!) { (response, status) in
//                self.answerSheetArray = response as! [ExamModel]
//                self.answerSheetTableView.reloadData()
//        }
        
        print("initView End")
    }

    func callAnswerSheetServiceWithAlamofire(teacherId: String, schoolId: String, courseID: String, completionHandler: @escaping ((Any, Bool) -> Void)) {
        let parameters: Parameters = ["teacherid": teacherId, "schoolid": schoolId, "courseid": courseID]
        
        AF.request(url, method: .post, parameters: parameters).validate().responseArray {
            (response: DataResponse<[ExamModel]>) in
            
            switch response.result {
            case .success:
                print("Validation Successful")
                if let objectArray = response.result.value {
                    completionHandler(objectArray, true)
                }
            case .failure(let error):
                completionHandler(error, false)
                print(error)
            }
        }
    }
    
    func callAnswerServiceWithAlamofire(courseID: String, completionHandler: @escaping ((Any, Bool) -> Void)) {
        let parameters: Parameters = ["courseid": courseID]
        
        AF.request(url, method: .post, parameters: parameters).validate().responseArray {
            (response: DataResponse<[ExamModel]>) in
            
            switch response.result {
            case .success:
                print("Validation Successful")
                if let objectArray = response.result.value {
                    completionHandler(objectArray, true)
                }
            case .failure(let error):
                completionHandler(error, false)
                print(error)
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ClassroomAnswerSheetViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("answerSheetArray.count = \(answerSheetArray.count)")
        return answerSheetArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath) as! ClassroomAnswerSheetTableViewCell
        print("tableView")
        cell.answerSheetLabel.text  = answerSheetArray[indexPath.row].examname
        print("cell.answerSheetLabel.text = \(String(describing: cell.answerSheetLabel.text))")
        return cell
    }
}
