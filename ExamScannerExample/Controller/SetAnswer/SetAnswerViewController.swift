//
//  SetAnswerViewController.swift
//  ExamScannerExample
//
//  Created by March Natkamon on 7/9/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire
import SideMenu

class SetAnswerViewController: UIViewController {
    
    var userObject: UserModel?
    var classroomArray = [ClassroomModel]()
    let url = "http://47.74.244.86/du_service_teacher/api/getClassroom"
    
    @IBOutlet var userImageView: ImageViewCircle!
    
    @IBOutlet var classroomTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        userObject = getDataUserDefault()
        initView()
//        print(user)
    }
    
    func getDataUserDefault() -> UserModel {
        let jsonData = UserDefaults.standard.object(forKey: "JSON_DATA")
        let user = Mapper<UserModel>().map(JSON: jsonData as! [String: Any])
        return user!
    }
    
    func initView() {
//        print("initView start")
        classroomTableView.delegate = self
        classroomTableView.dataSource = self
        
        let urlUserImage = URL(string: (userObject?.person_img)!)
//        print("url image = \(String(describing: URL(string: (userObject?.person_img)!)))")
        userImageView.kf.setImage(with: urlUserImage, placeholder: UIImage(named: "logo"),options: [
            .transition(.fade(1)),
            .cacheOriginalImage
        ])
        
        let teacherIdString = userObject?.personid
        let schoolIdString = userObject?.schoolid
//        print("t_id = \(teacherIdString!), s_id = \(schoolIdString!)")
        callClassroomServiceWithAlamofire(teacherId: teacherIdString!, schoolId: schoolIdString!) {
            (response, status) in
            
            self.classroomArray = response as! [ClassroomModel]
            self.classroomTableView.reloadData()
        }
//        print("initView end")
    }
    
    func callClassroomServiceWithAlamofire(teacherId: String, schoolId: String, completionHandler: @escaping ((Any, Bool) -> Void)) {
        let parameters: Parameters = ["teacherid": teacherId, "schoolid": schoolId]
        
        AF.request(url, method: .post, parameters: parameters).validate().responseArray {
            (response: DataResponse<[ClassroomModel]>) in
            
            switch response.result {
            case .success:
                print("Validation Successful")
                if let objectArray = response.result.value {
                    completionHandler(objectArray, true)
                }
            case .failure(let error):
                completionHandler(error, false)
                print(error)
            }
        }
//        print("callClassroomServiceWithAlamofire")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
    extension SetAnswerViewController: UITableViewDelegate, UITableViewDataSource {
        func  tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return classroomArray.count
        }
        
        func  tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath) as! SetAnswerTableViewCell
            cell.classroomLabel.text = classroomArray[indexPath.row].class_ta
            let gesture = UITapGestureRecognizer(target: self, action: #selector(clickCellBackground))
            cell.cellBackground.addGestureRecognizer(gesture)
            return cell
        }
        
        @objc func clickCellBackground(sender: UITapGestureRecognizer){
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "ClassroomAnswerSheetViewController") as! ClassroomAnswerSheetViewController
//            print("vc = \(vc)")
            
            let tapLocation = sender.location(in: self.classroomTableView)
//            print("tapLocation = \(tapLocation)")
            
            let indexPath = self.classroomTableView.indexPathForRow(at: tapLocation)
            
            vc.classroom = classroomArray[(indexPath?.row)!]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

