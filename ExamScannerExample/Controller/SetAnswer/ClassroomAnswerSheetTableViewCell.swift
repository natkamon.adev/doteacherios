//
//  ClassroomAnswerSheetTableViewCell.swift
//  ExamScannerExample
//
//  Created by Adev on 7/10/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

import UIKit

class ClassroomAnswerSheetTableViewCell: UITableViewCell {

    @IBOutlet var answerSheetLabel: UILabel!
    
    @IBOutlet var cellBackground: DesignableView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
