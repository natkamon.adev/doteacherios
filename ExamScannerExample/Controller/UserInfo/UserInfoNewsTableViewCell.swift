//
//  UserInfoNewsTableViewCell.swift
//  ExamScannerExample
//
//  Created by Nisit Sirimarnkit on 15/3/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

import UIKit

class UserInfoNewsTableViewCell: UITableViewCell {
    

    @IBOutlet var cellBackgroundView: DesignableView!
    
    @IBOutlet var newsImageView: UIImageView!
    
    @IBOutlet var titleLabel: UILabel!
        
    @IBOutlet var dateLabel: UILabel!
    
    @IBOutlet var detailTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func layoutSubviews() {
        super.layoutSubviews()
       
        self.newsImageView.frame = CGRect(x:self.newsImageView.frame.origin.x,y:self.newsImageView.frame.origin.y,width:112,height:104)
        self.newsImageView.contentMode = .scaleAspectFit
        
    }
    
}
