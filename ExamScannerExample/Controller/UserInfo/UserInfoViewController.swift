//
//  UserInfoViewController.swift
//  ExamScannerExample
//
//  Created by Nisit Sirimarnkit on 9/3/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class UserInfoViewController: UIViewController {
    
    @IBOutlet var schoolLogoImageView: UIImageView!
    
    @IBOutlet var userImageView: UIImageView!
    
    @IBOutlet var nameLabel: UILabel!
    
    @IBOutlet var subjectLabel: UILabel!
    
    
    @IBOutlet var newsTableView: UITableView!
    let url = "http://47.74.244.86/du_service_teacher/api/getTop4News"
    var newsArray = [NewsModel]()
    var userObject: UserModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        userObject = getDataUserDefault()
        //print(userObject?.fname)
        initView()
    }
    
    func getDataUserDefault() -> UserModel {
        let jsonData = UserDefaults.standard.object(forKey: "JSON_DATA")
        let user = Mapper<UserModel>().map(JSON: jsonData as! [String : Any])
        return user!
    }
    
    func initView(){
        
        let logoImage = URL(string: (userObject?.school_img)!)
        let userImage = URL(string: (userObject?.person_img)!)
        schoolLogoImageView.kf.setImage(with: logoImage,
                                        placeholder: UIImage(named: "logo"),
                                        options: [
                                            //.processor(processor),
                                            //.scaleFactor(UIScreen.main.scale),
                                            .transition(.fade(1))
                                            //.cacheOriginalImage
            ])
        let gesture = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        userImageView.isUserInteractionEnabled = true
        userImageView.addGestureRecognizer(gesture)
        userImageView.kf.setImage(with: userImage,
                                        placeholder: UIImage(named: "logo"),
                                        options: [
                                            //.processor(processor),
                                            //.scaleFactor(UIScreen.main.scale),
                                            .transition(.fade(1))
                                            //.cacheOriginalImage
            ])
        nameLabel.text = "อาจารย์ : \((userObject?.fname)!) \((userObject?.lname)!)"
        subjectLabel.text = "โรงเรียน : \((userObject?.schoolname)!)"
      
        self.newsTableView.delegate = self
        self.newsTableView.dataSource = self
        
        let schoolIdString = userObject?.schoolid
        let teacherIdString = userObject?.personid
        callNewsServiceWithAlamofire(schoolId: schoolIdString!, teacherID: teacherIdString!) { (response, status) in
            self.newsArray = response as! [NewsModel]
            
            self.newsTableView.reloadData()
        }
        
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "UserDetailViewController") as! UserDetailViewController
        self.present(vc, animated: true, completion: nil)
    }

    func callNewsServiceWithAlamofire(schoolId: String, teacherID: String, completionHandler: @escaping ((Any,Bool)->Void)) {
        let parameters: Parameters = ["schoolid": schoolId, "advisorid": teacherID]
        
        AF.request(url, method: .post,parameters: parameters).validate().responseArray { (response:DataResponse<[NewsModel]>) in
            
            switch response.result {
            case .success:
                print("Validation Successful")
                if let objectArray = response.result.value {
                    //print(objectArray.count)
                    completionHandler(objectArray,true)
                }
            case .failure(let error):
                completionHandler(error,false)
                print(error)
            }
        }
        print("callNewsServiceWithAlamofire")
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//survey normal
//print("data : \(newsArray[indexPath.row].news_type)")



extension UserInfoViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("size : \(newsArray.count)")
        
        return newsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath) as! UserInfoNewsTableViewCell
        let urlImage = URL(string: newsArray[indexPath.row].news_img!)
       
//        print("urlImage = \(String(describing: urlImage))")
        cell.newsImageView.kf.setImage(with: urlImage,
                                       placeholder: UIImage(named: "logo"),
                                       options: [
                                        //.processor(processor),
                                        //.scaleFactor(UIScreen.main.scale),
                                        .transition(.fade(1)),
                                        //.cacheOriginalImage
            ])
        cell.titleLabel.text = newsArray[indexPath.row].title
//        cell.detailTextView.text = newsArray[indexPath.row].news_detail
        cell.detailTextView.text = newsArray[indexPath.row].news_detail
        print("detail: \(newsArray[indexPath.row].news_detail!)")
        cell.dateLabel.text = "วันที่ : \(newsArray[indexPath.row].date1!)"
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(clickCellBackground))
        cell.cellBackgroundView.addGestureRecognizer(gesture)
        return cell
    }
    
    @objc func clickCellBackground(sender : UITapGestureRecognizer){
        
        let tapLocation = sender.location(in: self.newsTableView)
        let indexPath = self.newsTableView.indexPathForRow(at: tapLocation)
        
        if(newsArray[indexPath!.row].news_type == "normal"){
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "NewsDetailViewController") as! NewsDetailViewController
                    let tapLocation = sender.location(in: self.newsTableView)
                    let indexPath = self.newsTableView.indexPathForRow(at: tapLocation)
                    vc.schoolId = newsArray[(indexPath?.row)!].schoolid
                    vc.newsId = newsArray[(indexPath?.row)!].newsid
                    self.present(vc, animated: true, completion: nil)
        }else if(newsArray[indexPath!.row].news_type == "survey"){
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "WebViewNewsViewController") as! WebViewNewsViewController
                    let tapLocation = sender.location(in: self.newsTableView)
                    let indexPath = self.newsTableView.indexPathForRow(at: tapLocation)
                    vc.detailNews = newsArray[(indexPath?.row)!].news_detail
                    self.present(vc, animated: true, completion: nil)
        }

    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 200
//    }
    
}
