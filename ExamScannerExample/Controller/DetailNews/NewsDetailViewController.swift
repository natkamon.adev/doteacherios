//
//  NewsDetailViewController.swift
//  ExamScannerExample
//
//  Created by Nisit Sirimarnkit on 16/3/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class NewsDetailViewController: UIViewController {

    let url = "http://47.74.244.86/du_service_teacher/api/getNews_detail"
    
    var userObject: UserModel?
    var schoolId: String?
    var newsId: String?
    
    @IBOutlet var userImageView: ImageViewCircle!
    
    @IBOutlet var headerNewLabel: UILabel!
    
    @IBOutlet var dateLabel: UILabel!
    
    @IBOutlet var newsImageView: UIImageView!
    
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var contentTextView: UITextView!
    
    @IBAction func clickBackButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        userObject = getDataUserDefault()
        initView()
    }
    
    func getDataUserDefault() -> UserModel {
        let jsonData = UserDefaults.standard.object(forKey: "JSON_DATA")
        let user = Mapper<UserModel>().map(JSON: jsonData as! [String : Any])
        return user!
    }
    
    func initView(){
        
        let userImage = URL(string: (userObject?.person_img)!)
        userImageView.kf.setImage(with: userImage,
                                  placeholder: UIImage(named: "logo"),
                                  options: [
                                    //.processor(processor),
                                    //.scaleFactor(UIScreen.main.scale),
                                    .transition(.fade(1))
                                    //.cacheOriginalImage
            ])
        
        callNewsDetailServiceWithAlamofire(schoolId: schoolId!, newsid: newsId!) { (response, status) in
            let newsDetailObject = response as! NewsDetailModel
//            self.headerNewLabel.text = newsDetailObject.title
            self.titleLabel.text = newsDetailObject.title
            self.contentTextView.text = newsDetailObject.news_detail
            self.dateLabel.text = "วันที่สร้างข่าวสาร: \(newsDetailObject.date!)"
            let newsUrl = URL(string: (newsDetailObject.news_img!))
            self.newsImageView.kf.setImage(with: newsUrl,
                                      placeholder: UIImage(named: "logo"),
                                      options: [
                                        //.processor(processor),
                                        //.scaleFactor(UIScreen.main.scale),
                                        .transition(.fade(1))
                                        //.cacheOriginalImage
                ])
        }
    }

    func callNewsDetailServiceWithAlamofire(schoolId:String,newsid:String,completionHandler:@escaping ((Any,Bool)->Void)) {
        
        let parameters: Parameters = ["newsid":newsid,"schoolid":schoolId]
        
        AF.request(url, method: .post,parameters: parameters).validate().responseArray { (response:DataResponse<[NewsDetailModel]>) in
            
            switch response.result {
            case .success:
                print("Validation Successful")
                if let objectArray = response.result.value {
                    //print(objectArray.count)
                    completionHandler(objectArray[0],true)
                }
            case .failure(let error):
                completionHandler(error,false)
                print(error)
            }
        }
        
    }
  
}
