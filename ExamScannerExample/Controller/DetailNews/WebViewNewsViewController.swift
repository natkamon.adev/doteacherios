//  Created by Adev on 15/10/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

import UIKit
import ObjectMapper
import WebKit

class WebViewNewsViewController: UIViewController {
    var userObject: UserModel?
    var detailNews: String?
    var schoolid : String = ""
    var personid : String = ""
    
    @IBOutlet var serveyWebView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userObject = getDataUserDefault()
        
        schoolid = userObject!.schoolid!
        personid = userObject!.personid!
        
        loadWebContent()
    }
    
    @IBAction func clickBackButton(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
    }
    
    func loadWebContent() {
        
        if let myURL = URL(string: "http://47.74.244.86/doedu/survey.aspx?schoolid=\(schoolid)&userid=\(personid)&surveyid=\(detailNews!)&who=te") {
            let myRequest = URLRequest(url: myURL)
            serveyWebView?.load(myRequest)
        }
    }
    
    
     func getDataUserDefault() -> UserModel {
    //        print("getDataUserDefault")
            let jsonData = UserDefaults.standard.object(forKey: "JSON_DATA")
    //        print("jsonData setasnwer= \(jsonData!)")
            let user = Mapper<UserModel>().map(JSON: jsonData as! [String: Any])
    //        print("user = \(user!)")
            return user!
        }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
