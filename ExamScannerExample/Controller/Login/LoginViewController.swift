//
//  LoginViewController.swift
//  ExamScannerExample
//
//  Created by Nisit Sirimarnkit on 7/3/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import Kingfisher

class LoginViewController: UIViewController {

    let url = "http://47.74.244.86/du_service_teacher/api/getLogin"
    var userObject: UserModel?
    
    @IBOutlet var usernameTextField: UITextField!
    
    @IBOutlet var passwordTextField: UITextField!
        
    @IBAction func clickLogin(_ sender: Any) {
        
        var user = usernameTextField.text
        var pass = passwordTextField.text
        print("\(user!),\(pass!)")
        user = "adev02"
        pass = "1234"
        
        print("Login Click")
        
        callLoginWithAlamofire(user: user!, pass: pass!) { (response, status) in
                
            self.userObject = response as? UserModel
            let jsonData = self.userObject?.toJSON()
            print("jsonData = \(jsonData!)")
            UserDefaults.standard.set(jsonData, forKey: "JSON_DATA")
            
            if (status == true && (self.userObject!.returncode == "1000")) {
                
                let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "UserInfoViewController") as! UserInfoViewController
                self.navigationController?.pushViewController(vc, animated: true)
                
            } else {
                let alertController = UIAlertController(title: "เกิดข้อผิดพลาด", message: "ไม่สามารถล็อกอินได้ กรุณาลองใหม่อีกครั้ง.", preferredStyle: .alert)
                
                let action1 = UIAlertAction(title: "ตกลง", style: .default) { (action:UIAlertAction) in
                    print("You've pressed default");
                }
                
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    
    @IBAction func clickForgotPassword(_ sender: Any) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }
    
    func initView() {
        usernameTextField.borderStyle = .roundedRect
        passwordTextField.borderStyle = .roundedRect
    }
    
    func callLoginWithAlamofire(user:String,pass:String,completionHandler:@escaping (_ isResponse:Any,Bool)-> Void) {
        let parameters: Parameters = ["username": user,"password": pass]
        
        AF.request(url,method: .post,parameters: parameters).validate().responseObject { (response:DataResponse<UserModel>) in
            
            switch response.result {
            case .success:
                print("Validation Successful")
                if let object = response.result.value {
                    completionHandler(object,true)
                }
            case .failure(let error):
                completionHandler(error,false)
                print(error)
            }
            
        }
        
    }

}

