//
//  ForgotPasswordViewController.swift
//  ExamScannerExample
//
//  Created by Adev on 15/9/2562 BE.
//  Copyright © 2562 xAdmin. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire
import AlamofireObjectMapper
import Kingfisher

class ForgotPasswordViewController: UIViewController {
    var userObject: UserModel?
    let url = "http://47.74.244.86/du_service_teacher/api/Req_reset_pass"
    
    
    @IBOutlet var usernameTextField: UITextField!
    
    @IBOutlet var telNoTextField: UITextField!
    
    @IBAction func sendData(_ sender: Any) {
        let user = usernameTextField.text
        let tel = telNoTextField.text
        print("username: \(user!), tel: \(tel!)")
        
        
        
        callLoginWithAlamofire(user: user!, tel: tel!) { (response, status) in
                
            self.userObject = response as? UserModel
            let jsonData = self.userObject?.toJSON()
            print("jsonData = \(jsonData!)")
            
            UserDefaults.standard.set(jsonData, forKey: "JSON_DATA")
            if (status == true && (self.userObject!.returncode == "1000")) {

                let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else {
                let alertController = UIAlertController(title: "เกิดข้อผิดพลาด", message: "ข้อมูลไม่ถูกต้อง", preferredStyle: .alert)

                let action1 = UIAlertAction(title: "ตกลง", style: .default) { (action:UIAlertAction) in
                    print("You've pressed default");
                }

                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
            }
            
        }
    }
    
    
    func callLoginWithAlamofire(user: String, tel: String, completionHandler:@escaping (_ isResponse:Any,Bool)-> Void) {
        let parameters: Parameters = ["username": user,"tel": tel]
        
        AF.request(url,method: .post,parameters: parameters).validate().responseObject { (response:DataResponse<UserModel>) in
            
            switch response.result {
            case .success:
                print("Validation Successful")
                if let object = response.result.value {
                    completionHandler(object,true)
                }
            case .failure(let error):
                completionHandler(error,false)
                print(error)
            }
            
        }
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        userObject = getDataUserDefault()
    }
    
    func initView() {
        usernameTextField.borderStyle = .roundedRect
        telNoTextField.borderStyle = .roundedRect
    }
    
    func getDataUserDefault() -> UserModel {
        let jsonData = UserDefaults.standard.object(forKey: "JSON_DATA")
        let user = Mapper<UserModel>().map(JSON: jsonData as! [String: Any])
        return user!
        }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
