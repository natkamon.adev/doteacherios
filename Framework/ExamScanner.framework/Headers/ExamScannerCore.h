//
//  ExamScannerCore.h
//  ExamScanner
//
//  Created by xAdmin on 29/10/2561 BE.
//  Copyright © 2561 xAdmin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ScanInfo.h"
#import "ScanResult.h"
#import "ScanConfig.h"

//#define ES_DEBUG

@class ExamScanner;
@protocol ExamScannerDelegate <NSObject>
- (void)examScannerDidSuccessScan: (ExamScanner* _Nonnull) sender withResult:(ScanResult* _Nonnull)result ;
@end

@interface ExamScanner : NSObject

- (instancetype _Nonnull)init NS_UNAVAILABLE;
- (instancetype _Nullable)initWithConfig: (ScanConfig* _Nonnull)config;
- (ScanInfo *_Nullable)scan: (UIImage *_Nonnull)image;

@property (weak, nonatomic, nullable) id<ExamScannerDelegate> delegate;
@property (nonatomic) bool isProcessing;
@end

