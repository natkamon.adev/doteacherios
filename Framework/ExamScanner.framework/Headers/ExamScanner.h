//
//  ExamScanner.h
//  ExamScanner
//
//  Created by xAdmin on 29/10/2561 BE.
//  Copyright © 2561 xAdmin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ExamScanner/ExamScannerCore.h>
#import <ExamScanner/ScanInfo.h>
#import <ExamScanner/ScanResult.h>
#import <ExamScanner/ScanConfig.h>
////! Project version number for ExamScanner.
FOUNDATION_EXPORT double ExamScannerVersionNumber;
//
////! Project version string for ExamScanner.
FOUNDATION_EXPORT const unsigned char ExamScannerVersionString[];
//
//// In this header, you should import all the public headers of your framework using statements like #import <ExamScanner/PublicHeader.h>


