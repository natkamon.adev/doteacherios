//
//  ScanInfo.h
//  ExamScanner
//
//  Created by xAdmin on 29/10/2561 BE.
//  Copyright © 2561 xAdmin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ScanInfo : NSObject
@property (nonatomic, retain) UIImage* bufferImage;
@property (nonatomic, retain) UIImage* debugImage1;
@property (nonatomic, retain) UIImage* debugImage2;
@property (nonatomic, retain) UIImage* debugImage3;
@property (nonatomic, retain) UIImage* debugImage4;
@end
