//
//  ScanResult.h
//  ExamScanner
//
//  Created by xAdmin on 29/10/2561 BE.
//  Copyright © 2561 xAdmin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ScanResult : NSObject
@property (nonatomic, retain) NSArray<NSNumber*>* resultAnswers;
@property (nonatomic, retain) NSArray<NSNumber*>* resultStudentID;
@property (nonatomic, assign) int score;
@property (nonatomic, retain) UIImage* overlayResultImage;
@end
