//
//  ScanConfig.h
//  ExamScanner
//
//  Created by xAdmin on 29/10/2561 BE.
//  Copyright © 2561 xAdmin. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, ScanTemplateType) {
    exam_50_choice,
    exam_100_choice,
};

@interface ScanConfig : NSObject
@property (nonatomic) ScanTemplateType templateType;
@property (nonatomic, retain) NSArray<NSNumber*>* answers;
@end
